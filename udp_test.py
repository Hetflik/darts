import socket
#import pyttsx3
import math
import time
import string
import random


localIP     = ""
serverIP    = ""
localPort   = 5420
bufferSize  = 1024


def getLocalIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0].split('.', 4)

class UDPTester:
    def __init__(self, ipAddress, port, timeout):
        self.udp_ip = ipAddress
        self.port = port
        self.timeout = timeout
        self.connected = False
        self.key = "darts"
            
    def broadcastUDP(self):
        MESSAGE = self.key.encode()

        sock = socket.socket(socket.AF_INET,    # Internet
                            socket.SOCK_DGRAM)  # UDP
        sock.sendto(MESSAGE, (self.udp_ip, self.port))
        sock.settimeout(self.timeout)
        
        try:
            data = None
            data, remoteIP = sock.recvfrom(4096)
        except socket.timeout:
            pass

        if data is not None:
            #if data.decode() == self.key[::-1]:
            print("Packet received: {}".format(data.decode()))
            print("Key is : {}".format(self.key))
            if data.decode() == self.key:
                self.connected = True
                print("\nConnected to {}".format(remoteIP[0]))
            else:
                print("Packet received: {}".format(data.decode()))
            
def main():
    localIP = getLocalIP()
    
    if 1 == 1:
        success = False
        scanIP = [0,0,0,0]
        scanIP[0] = localIP[0]
        scanIP[1] = localIP[1]
        scanIP[2] = localIP[2]

        while not success:
            print("Waiting for server...")
            for i in range(1, 255):
                scanIP[3] = str(i)
                serverIP = ('.').join(scanIP)
                darts = UDPTester(serverIP, localPort, .1)
                darts.broadcastUDP()
    #           print(".", end = "", flush=True)
                if darts.connected:
                    print("Success!")
                    success = True
                    break
        
    while True:
        sock = socket.socket(socket.AF_INET,    # Internet
                            socket.SOCK_DGRAM)  # UDP
        server_address = ('.'.join(localIP), localPort)
        sock.bind(server_address)
        data = sock.recvfrom(4096)
        if len(data[0].decode()) == 1:
            print("Packet received: {}".format(ord(data[0].decode())))
        else:
            print("Packet received: {}".format(data[0].decode()))

if __name__ == "__main__":
    main()

