 
#ifndef DEBUGPRINT_H
#define DEBUGPRINT_H

#define DEBUG

#ifdef DEBUG
  #define debugPrint(x)  Serial.print (x)
  #define debugPrintLn(x)  Serial.println (x)
#else
  #define serialPrint(x)
#endif

#endif
