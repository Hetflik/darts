 
int masterLines = 8; //Change here to the number of lines of your Master Layer
int slaveLines = 8; //Change here to the number of lines of your Slave Layer
int matrixMaster[] = {13, 12, 11, 10, 9, 8, 7, 6}; //Put here the pins you connected the lines of your Master Layer 
int matrixSlave[] = {5, 4, 3, 2, A5, A4, A3, A2}; //Put here the pins you connected the lines of your Slave Layer
int dartPointInt = 0;
char dartPointChar = "";

void setup() {
    Serial.begin(9600);
    for(int i = 0; i < slaveLines; i++){
        pinMode(matrixSlave[i], INPUT_PULLUP);
    }
    for(int i = 0; i < masterLines; i++){
       pinMode(matrixMaster[i], OUTPUT);
       digitalWrite(matrixMaster[i], HIGH);
    }
}

void loop() {
    for(int i = 0; i < masterLines; i++){
        digitalWrite(matrixMaster[i], LOW);
        for(int j = 0; j < slaveLines; j++){
            if(digitalRead(matrixSlave[j]) == LOW){
                dartPointInt = 10 * i + j;
                dartPointChar = (char)dartPointInt;
                Serial.print(dartPointChar);
                delay(500);
                break;
            }
        }
        digitalWrite(matrixMaster[i], HIGH);
    }
}
