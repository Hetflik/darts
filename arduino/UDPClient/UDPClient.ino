#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "DebugPrint.h"

WiFiUDP Udp;
IPAddress ipAddress;

unsigned int localPort = 5040;
char const key[6] = "darts";
char packet[6];

// Gets UDP packets
bool packetReceived()
{
    bool returnValue = false;
    int udpPacket = Udp.parsePacket();
    if (udpPacket) 
    {
        int len = Udp.read(packet, 6);
        if (len > 0) 
        {
            packet[len] = '\0';
        }

        debugPrint("Packet received: ");
        debugPrintLn(packet);
        returnValue = true;
    }
    
    return returnValue;
}


void modifyPacket()
{
    reverse(packet, packet + strlen(packet));
    return;
}

bool serverConnected()
{
    if (packetReceived()) 
    {
        if (strcmp(packet, key) != 0)
        {
            debugPrintLn("Invalid key!");
            return false;
        }
        
        Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
        Udp.write(packet);
        Udp.endPacket();
        debugPrintLn("Client connected. Enjoy!");
        
        return true;
    }
    
    return false;
}

void setup() 
{
    Serial.begin(9600);
    debugPrintLn();

    WiFi.begin("Chuafuj", "Bh9_2jf182");

    // Wifi connection
    debugPrint("Connecting...");
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        debugPrint(".");
    }
    debugPrintLn();

    ipAddress = WiFi.localIP();
    debugPrint("Connected, IP address: ");
    debugPrintLn(WiFi.localIP());

    Udp.begin(localPort);
    debugPrint("Listening on UDP port ");
    debugPrintLn(localPort);

    // Server connection
    debugPrintLn("Waiting for client...");
    while (!serverConnected()) 
    {
        delay(500);
        debugPrint(".");
    } 
}

void loop() 
{
    Udp.beginPacket(ipAddress, localPort);
    Udp.write("Hello");
    Udp.endPacket();
    delay(1000);
}
