
class Player:

    idCounter = 0
    playerPoints = 0

    @classmethod
    def __getPlayerId(self):
        self.idCounter += 1
        return self.idCounter

    def __getDefaultColor(self, id):
        switch = {
            1: "blue",
            2: "red",
            3: "orange",
            4: "green"
        }
        return switch.get(id, "white")

    def __init__(self, name, color = None):
        id = self.__getPlayerId()
        color = self.__getDefaultColor(id)

        self.id = id
        self.name = name
        self.color = color

        self.idCounter += 1
