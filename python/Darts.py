#
#   pyDarts
#

from GameTypes.Game import *
from GameTypes.ZeroOne import *
from Player import *
from Udp import *


class Darts:
    def __init__(self, master = [None]*8, slave = [None]*8):
        self.master = master
        self.slave = slave
    
    def __indexSep(self, value):
        self.i = math.floor(value / 10)
        self.j = value % 10
        return (self.i, self.j)
    
    def __indexComb(self, i, j):
        index = 10 * masterMatrix[i] + slaveMatrix[j]
        return index

    def __setMatrix(self, step, value):
        index_sep = self.__indexSep(value)
        self.master[step] = index_sep[0]
        self.slave[step] = index_sep[1]

    def getTargetValue(self, value):
        switcher = {
            self.__indexComb(6,2): 1,
            self.__indexComb(3,7): 2,
            self.__indexComb(3,3): 3,
            self.__indexComb(1,3): 4,
            self.__indexComb(6,7): 5,
            self.__indexComb(2,2): 6,
            self.__indexComb(3,6): 7,
            self.__indexComb(6,5): 8,
            self.__indexComb(6,3): 9,
            self.__indexComb(3,2): 10,
            self.__indexComb(6,6): 11,
            self.__indexComb(6,1): 12,
            self.__indexComb(1,2): 13,
            self.__indexComb(6,4): 14,
            self.__indexComb(3,0): 15,
            self.__indexComb(3,5): 16,
            self.__indexComb(3,1): 17,
            self.__indexComb(2,3): 18,
            self.__indexComb(3,4): 19,
            self.__indexComb(6,0): 20,
            self.__indexComb(2,0): 25,
            self.__indexComb(0,2): "Double! 1",
            self.__indexComb(4,7): "Double! 2",
            self.__indexComb(4,3): "Double! 3",
            self.__indexComb(1,4): "Double! 4",
            self.__indexComb(0,7): "Double! 5",
            self.__indexComb(2,6): "Double! 6",
            self.__indexComb(4,6): "Double! 7",
            self.__indexComb(0,5): "Double! 8",
            self.__indexComb(0,3): "Double! 9",
            self.__indexComb(4,2): "Double! 10",
            self.__indexComb(0,6): "Double! 11",
            self.__indexComb(0,1): "Double! 12",
            self.__indexComb(1,6): "Double! 13",
            self.__indexComb(0,4): "Double! 14",
            self.__indexComb(4,0): "Double! 15",
            self.__indexComb(4,5): "Double! 16",
            self.__indexComb(4,1): "Double! 17",
            self.__indexComb(2,4): "Double! 18",
            self.__indexComb(4,4): "Double! 19",
            self.__indexComb(0,0): "Double! 20",
            self.__indexComb(1,0): "Bullseye",
            self.__indexComb(7,2): "Triple! 1",
            self.__indexComb(5,7): "Triple! 2",
            self.__indexComb(5,3): "Triple! 3",
            self.__indexComb(1,1): "Triple! 4",
            self.__indexComb(7,7): "Triple! 5",
            self.__indexComb(2,7): "Triple! 6",
            self.__indexComb(5,6): "Triple! 7",
            self.__indexComb(7,5): "Triple! 8",
            self.__indexComb(7,3): "Triple! 9",
            self.__indexComb(5,2): "Triple! 10",
            self.__indexComb(7,6): "Triple! 11",
            self.__indexComb(7,1): "Triple! 12",
            self.__indexComb(1,7): "Triple! 13",
            self.__indexComb(7,4): "Triple! 14",
            self.__indexComb(5,0): "Triple! 15",
            self.__indexComb(5,5): "Triple! 16",
            self.__indexComb(5,1): "Triple! 17",
            self.__indexComb(2,1): "Triple! 18",
            self.__indexComb(5,4): "Triple! 19",
            self.__indexComb(7,0): "Triple! 20"
        }
        a = switcher.get(value, "Error")
        return a

# Define initial slave and master Matrix
masterMatrix    = [0,7,6,4,5,3,1,2]
slaveMatrix     = [5,7,4,3,2,0,1,6]

darts = Darts(masterMatrix, slaveMatrix)
connection = UDP()
connection.testUDP()

player1 = Player("Player 1")
player2 = Player("Player 2")
players = [player1, player2]

a = ZeroOne(players, 301)

a.nextPlayer()




