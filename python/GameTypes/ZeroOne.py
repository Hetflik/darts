
from .Game import *

class ZeroOne(Game):
    
    def __init__(self, players, points):
        super().__init__(players)
        self.points = points
        self.gameType = points
        
    @property
    def gameType(self):
        return self._gameType
    
    @gameType.setter
    def gameType(self, value):
        if value != 301:
            raise ValueError("Invalid ZeroOne game points.")
        self._gameType = str(value)
        
    def targetHit(self, value):
        self.points -= value

    def printPoints(self):
        print(self.points)
