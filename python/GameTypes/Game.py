

class Game:
    
    def __init__(self, players):
        self.activePlayer = 0
        self.players = players

    def nextPlayer(self):
        if self.activePlayer == len(self.players) - 1:
            self.activePlayer = 0
        else:
            self.activePlayer += 1
        print("Player {}".format(self.players[self.activePlayer].name))
        
        
    def targetHit(self, value):
        """Defines behaviour after target hit."""
        pass
