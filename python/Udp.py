import socket

import math
import time


class UDP:
    def __init__(self):
        self.timeout = .1
        self.connected = False
        self.key = "darts"
        self.localIp = None
        self.serverIp = None
        self.localPort   = 5420
            
    def getLocalIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0].split('.', 4)
            
            
    def broadcastUDP(self):
        MESSAGE = self.key.encode()

        sock = socket.socket(socket.AF_INET,    # Internet
                            socket.SOCK_DGRAM)  # UDP
        sock.sendto(MESSAGE, (self.serverIP, self.localPort))
        sock.settimeout(self.timeout)
        
        try:
            data = None
            data, remoteIP = sock.recvfrom(1024)
        except socket.timeout:
            pass

        if data is not None:
            #if data.decode() == self.key[::-1]:
            print("Packet received: {}".format(data.decode()))
            print("Key is : {}".format(self.key))
            if data.decode() == self.key:
                self.connected = True
                print("\nConnected to {}".format(remoteIP[0]))
            else:
                print("Packet received: {}".format(data.decode()))
            
    def testUDP(self):
        localIP = self.getLocalIP()
        
        print(localIP)
        
        if 1 == 1:
            success = False
            scanIP = [0, 0, 0, 0]
            scanIP[0] = localIP[0]
            scanIP[1] = localIP[1]
            scanIP[2] = localIP[2]

            while not success:
                print("Waiting for server...")
                for i in range(1, 255):
                    scanIP[3] = str(i)
                    self.serverIP = ('.').join(scanIP)
                    #udpConnect = UDPTester(serverIP, darts.localPort, .1)
                    self.broadcastUDP()
                    if self.connected:
                        sock = socket.socket(socket.AF_INET,    # Internet
                                            socket.SOCK_DGRAM)  # UDP
                        server_address = ('.'.join(localIP), self.localPort)
                        sock.bind(server_address)      
                        success = True
                        break
            
        while True:
            data = sock.recvfrom(1024)
            
            if len(data[0].decode()) == 1:
                print("Packet received: {}".format(ord(data[0].decode())))   
            
            else:
                print("Packet received: {}".format(data[0].decode()))    
                
        #    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        #   message = bytesAddressPair[0]
        #  msg = int.from_bytes(message, 'little')

